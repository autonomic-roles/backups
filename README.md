autonomic-roles/backups
=======================

Set up `borg` backups using `backupninja` and our wonderful `pass`-based credentials service.

`backupninja` is used because it means that you can drop config files into /etc/backup.d to set up more things to back up - like, for example, MySQL.  There is a tool, `ninjahelper`, which can help generate these config files.  They all have excellent `man` pages.  [Project page (the README is worth reading)](https://0xacab.org/riseuplabs/backupninja/).

Notes
-----

This role currently fetches a branch of `backupninja` to get borg support, and then patches it to allow specifying the port used.  Hopefully this will be redundant at some point.  (The patch has been submitted upstream.)


Requirements
------------

You need to make sure that Ansible can find the password store.  You do this by setting the environment variable `PASSWORD_STORE` to the right path.

Role Variables
--------------

You need to set:

### `backup_server`

```
backup_server: hetzner-external
```

This will look in the password store, under `backups/<backup_server>/(host|password|username)`, for login and credential information.

### `backup_scripts`

```
backup_scripts:
- 30.mysql
- 40.pgsql
```

If a list of filenames is provided, then these files will be copied from `templates/backup/` into `/etc/backup.d`.

### `server_name`

```
server_name: dangermouse
```

This is used to find or create the appropriate borg repokey.   This role will look at `backups/repokeys/<server_namename>` for the repo key, creating one if there isn't one there already.  (**Remember to commit this!**)

### `borg_include_paths`

default:
```
borg_include_paths:
 - /var/spool/cron/crontabs
 - /var/backups
 - /var/www
 - /etc
 - /root
```

What to back up.

### `borg_exclude_paths`

default:
```
borg_exclude_paths:
 - /home/*/.cache
```

What not to back up.


Example Playbook
----------------

```
	- hosts: servers
	  vars:
	    server_name: dangermouse
	  roles:
	  - { role: autonomic-roles.backups, backup_server: 'hetzner_external', }
```
