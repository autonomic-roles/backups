#!/bin/bash

cd /root/backupninja

set -e
set -x

PREFIX=/usr/local/Cellar/backupninja
VAR=/var
ETC=/etc

./autogen.sh
./configure --prefix=$PREFIX --localstatedir=$VAR --sysconfdir=$ETC
make clean
make
make install

# Apply the port patch

cd /usr/local/Cellar/backupninja/share/backupninja

# Link in man pages & sbin stuff
ln -sf $PREFIX/sbin/ninjahelper                      /usr/sbin/ninjahelper
ln -sf $PREFIX/sbin/backupninja                      /usr/sbin/backupninja
ln -sf $PREFIX/share/man/man1/ninjahelper.1          /usr/share/man/man1/ninjahelper.1
ln -sf $PREFIX/share/man/man1/backupninja.1          /usr/share/man/man1/backupninja.1
ln -sf $PREFIX/share/man/man5/backup.d.5             /usr/share/man/man5/backup.d.5
ln -sf $PREFIX/share/man/man5/backupninja.conf.5     /usr/share/man/man5/backupninja.conf.5

# Make cron job executable
chmod u+x $ETC/cron.d/backupninja
