#!/bin/bash

cd /root

# Stolen from https://stackoverflow.com/questions/39530748/bash-check-if-file-contains-other-file-contents
catAndAppendIfMissing()
{
	f1=$(wc -c < "$1")
	diff  -y <(od -An -tx1 -w1 -v "$1") <(od -An -tx1 -w1 -v "$2") | \
	rev | cut -f2 | uniq -c | grep -v '[>|]' | numgrep /${f1}../ | \
	grep -q -m1 '.+*' || cat "$1" >> "$2";
}

export SSHPASS={{ backup_pw }}
export HOSTSPEC={{ backup_user }}@{{ backup_host }}

set -e
set -x

ssh-keygen -e -f .ssh/id_rsa.pub | grep -v "Comment:" > .ssh/id_rsa_rfc.pub

echo get .ssh/authorized_keys /root/auth_original \
	| sshpass -e sftp -o StrictHostKeyChecking=no $HOSTSPEC

# Sometimes the remote server won't have any authorised keys yet
if  [ ! -e /root/auth_original ]; then
	touch /root/auth_original
	echo mkdir .ssh \
		| sshpass -e sftp -o StrictHostKeyChecking=no $HOSTSPEC
fi

# Copy auth_original to auth_new
cp /root/auth_original /root/auth_new
catAndAppendIfMissing .ssh/id_rsa_rfc.pub /root/auth_new
catAndAppendIfMissing .ssh/id_rsa.pub /root/auth_new

cmp /root/auth_original /root/auth_new || \
	(echo put /root/auth_new .ssh/authorized_keys \
		| sshpass -e sftp -o StrictHostKeyChecking=no $HOSTSPEC)

rm auth_new auth_original
